package com.kiddoware.speedmathandroid;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MathActivity extends AppCompatActivity {

    private RadioButton rb10;
    private RadioButton rb20;
    private RadioButton rb30;
    private RadioButton rb40;
    private SeekBar seek_bar;
    private TextView text_view;
    private TextView textViewQuestion;
    private  TextView textViewAnswer;
    private  TextView textViewMsg;
    private TextView textViewCounter;
    private EditText editTextAnswer;
    private TextView textViewOperation;
    private Button btnSubmit1;
    private Button btnSubmit2;
    private Button btnSubmit3;
    private Button btnSubmit4;
    public Button resultbutton;
    private int noOfQuestions;
    private TextView textViewCorrect;
    private TextView textViewTotal;
    public int correctAnsCount = 0;
    private int questionsShownCount = 0;
    int correctAnswer;
    int m=10;
    boolean isCounterRunning = false;

    String operation;
    CountDownTimer mCountDownTimer = new CountDownTimer(10000, 1000) {

        @Override
        public void onTick(long millisUntilFinished) {

                textViewCounter.setText(getResources().getString(R.string.secondsRemaining, String.valueOf(millisUntilFinished / 1000)));


        }

        @Override
        public void onFinish() {
            isCounterRunning = false;
            textViewCounter.setText("done!");
            if(questionsShownCount == noOfQuestions){
                Toast.makeText(getApplicationContext(),"quiz over",Toast.LENGTH_LONG).show();;
            }else {
                showQuestionAnswers();
            }
        }
    };

    public void seek(){
        seek_bar = (SeekBar)findViewById(R.id.seekBar);
        text_view = (TextView)findViewById(R.id.textView);
        text_view.setText("Range: "+seek_bar.getProgress()+"/"+seek_bar.getMax());

        seek_bar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress_value;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        progress_value=i;
                        text_view.setText("Range: "+ i +"/"+seek_bar.getMax());
                        if(progress_value == 0){
                            progress_value = progress_value+1;
                        }
                        m=progress_value;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        text_view.setText("Range: "+progress_value+"/"+seek_bar.getMax());
                    }
                }
        );
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution);
        seek();
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            operation = extras.getString("operation");
        }

        rb10 = (RadioButton)findViewById(R.id.radioButton10);
        rb20 = (RadioButton)findViewById(R.id.radioButton20);
        rb30 = (RadioButton)findViewById(R.id.radioButton30);
        rb40 = (RadioButton)findViewById(R.id.radioButton40);
        textViewQuestion = (TextView)findViewById(R.id.textViewQuestion);
        textViewAnswer = (TextView)findViewById(R.id.textViewAnswer);
        textViewMsg = (TextView)findViewById(R.id.textViewMsg);
        textViewCounter = (TextView)findViewById(R.id.textViewCounter);
        btnSubmit1 = (Button)findViewById(R.id.buttonSubmit1);
        btnSubmit2 = (Button)findViewById(R.id.buttonSubmit2);
        btnSubmit3 = (Button)findViewById(R.id.buttonSubmit3);
        btnSubmit4 = (Button)findViewById(R.id.buttonSubmit4);
        resultbutton= (Button)findViewById(R.id.ResultButton);
        textViewOperation = (TextView) findViewById(R.id.OperationText);
        textViewOperation.setText(operation);
        editTextAnswer = (EditText)findViewById(R.id.editTextAnswer);
        textViewCorrect = (TextView) findViewById(R.id.textViewCorrect);
        textViewTotal = (TextView) findViewById(R.id.textViewTotal);


    }



    public void onRbClick(View v) {
        if(v.getId() == R.id.radioButton10){
            noOfQuestions = 10;
            rb20.setSelected(false);
            rb30.setSelected(false);
            rb40.setSelected(false);
        }
        else if(v.getId() == R.id.radioButton20){
            noOfQuestions = 20;
            rb10.setSelected(false);
            rb30.setSelected(false);
            rb40.setSelected(false);
        }
        else if(v.getId() == R.id.radioButton30){
            noOfQuestions = 30;
            rb20.setSelected(false);
            rb10.setSelected(false);
            rb40.setSelected(false);
        }
        else if(v.getId() == R.id.radioButton40){
            noOfQuestions = 40;
            rb20.setSelected(false);
            rb30.setSelected(false);
            rb10.setSelected(false);
        }
        correctAnsCount=0;
        questionsShownCount=0;
        textViewCorrect.setText("");
        showQuestionAnswers();

    }

    private void showQuestionAnswers() {
        int firstNum = randInt(1,m);
        int secondNum = randInt(1,m);
        //to make sure firt number in subtraction is always bigger than second
        if (firstNum < secondNum){
            int temp = firstNum;
            firstNum = secondNum;
            secondNum = temp;
        }
        //to make sure division is always a whole number
        if(getOperationSign().equals("/")){
            int tempfirstNum=firstNum*secondNum;
            correctAnswer=firstNum;
            firstNum=tempfirstNum;
        }
        else {
            correctAnswer = getCorrectAnswer(firstNum, secondNum, getOperationSign());
        }

        textViewQuestion.setText("Question: " + firstNum + " " + getOperationSign() + " " + secondNum +  "  =  ? " );
        textViewQuestion.setVisibility(View.VISIBLE);
        textViewAnswer.setVisibility(View.VISIBLE);
        //editTextAnswer.setVisibility(View.VISIBLE);
        btnSubmit1.setVisibility(View.VISIBLE);
        btnSubmit2.setVisibility(View.VISIBLE);
        btnSubmit3.setVisibility(View.VISIBLE);
        btnSubmit4.setVisibility(View.VISIBLE);
        questionsShownCount = questionsShownCount + 1;
        textViewTotal.setText(questionsShownCount + "/" + noOfQuestions);
        showAnserOptions(correctAnswer);
        startCountDown();
    }
    private String getOperationSign(){
        String operationSign = null;
        if (operation.equals("Add")) {

            operationSign = "+";
        }
        else if (operation.equals("Subtract")) {
            operationSign = "-";

        }
        else if (operation.equals("Multiplication")) {
            operationSign = "X";

        }
        else if (operation.equals("Division")) {
            operationSign = "/";

        }
        return  operationSign;

    }

    private int getCorrectAnswer(int firstNumber,int secondNumber, String operationSign){
        int answer = 0;
        if(operationSign.equals("+")){
            answer = firstNumber +secondNumber;
        }
        else if(operationSign.equals("-")){
            answer = firstNumber - secondNumber;
        }
        else if(operationSign.equals("X")){
            answer = firstNumber * secondNumber;
        }
        else if(operationSign.equals("/")){
            answer = firstNumber / secondNumber;
        }
        return answer;
    }

    private void showAnserOptions(int correctAnswer) {
        try {
            int randCorrectButton = randInt(1, 4);
            for (int i = 1; i < 5; i++) {

                if (i != randCorrectButton) {
                    int randAnswer = randInt(0, 20);
                    if (randAnswer == correctAnswer) {
                        randAnswer = randAnswer + randInt(1, 4);
                    }
                    if (i == 1) {
                        btnSubmit1.setText(Integer.toString(randAnswer));
                    }
                    if (i == 2) {
                        btnSubmit2.setText(Integer.toString(randAnswer));
                    }
                    if (i == 3) {
                        btnSubmit3.setText(Integer.toString(randAnswer));
                    }
                    if (i == 4) {
                        btnSubmit4.setText(Integer.toString(randAnswer));
                    }
                } else {
                    if (randCorrectButton == 1) {
                        btnSubmit1.setText(Integer.toString(correctAnswer));
                    }
                    if (randCorrectButton == 2) {
                        btnSubmit2.setText(Integer.toString(correctAnswer));
                    }
                    if (randCorrectButton == 3) {
                        btnSubmit3.setText(Integer.toString(correctAnswer));
                    }
                    if (randCorrectButton == 4) {
                        btnSubmit4.setText(Integer.toString(correctAnswer));
                    }
                }

            }
        }
        catch(Exception ex){
            String a = "b";
        }

    }

    public void onSubmitClick (View v){
        String answer = ((Button)v).getText().toString();

        try {
            int userAnswer = Integer.parseInt(answer);
            if(userAnswer == correctAnswer){
                textViewMsg.setText("Correct");
                if(questionsShownCount<=noOfQuestions) {

                    correctAnsCount = correctAnsCount + 1;

                }
                textViewCorrect.setText("Correct Answers: "+ correctAnsCount);
            }
            else {
                textViewMsg.setText("In Correct");
            }
            textViewMsg.setVisibility(View.VISIBLE);

        }
        catch (Exception ex){
            textViewMsg.setText("In Correct");
        }
        if(questionsShownCount == noOfQuestions){
            Toast.makeText(getApplicationContext(),"quiz over",Toast.LENGTH_LONG).show();;
        }
        else{

            showQuestionAnswers();
        }

        Animation animation = AnimationUtils.loadAnimation(MathActivity.this, R.anim.rotate);
        TextView tv = (TextView)findViewById(R.id.textViewCounter);
        tv.startAnimation(animation);


    }

    public void onResultClick(View v){

        Intent next = new Intent(MathActivity.this, ResultActivity.class);
        startActivity(next);



    }



    /**
     * Returns a psuedo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimim value
     * @param max Maximim value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public static int randInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    private void startCountDown(){
        mCountDownTimer.start();
        isCounterRunning = true;
    }
    private void stopCountDown(){
        if(isCounterRunning){
            mCountDownTimer.cancel();
            textViewCounter.setText("");
        }

    }
    }
