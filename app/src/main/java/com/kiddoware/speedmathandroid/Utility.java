package com.kiddoware.speedmathandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Utility {

    public static final String KEY_FIRST_TIME_SETTING = "firstTimeValue";//
    public static final String KEY_INSTALL_DATE_SETTING = "installDateValue";//
    public static final String KEY_MIN_NO_RANGE_SETTING = "minNumberRangeValue";//



    public static boolean getFirstTimeSetting(Context ctxt) {
        boolean value = false;
        try {
            SharedPreferences settings = PreferenceManager
                    .getDefaultSharedPreferences(ctxt);
            value = settings.getBoolean(KEY_FIRST_TIME_SETTING, true);
        } catch (Exception ex) {

        }
        return value;
    }

    public static void setFirstTimeSetting(Context ctxt, boolean value) {
        try {
            // Save user preferences. We need an Editor object to
            // make changes. All objects are from android.context.Context
            SharedPreferences settings = PreferenceManager
                    .getDefaultSharedPreferences(ctxt);

            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(KEY_FIRST_TIME_SETTING, value);
            editor.putLong(KEY_INSTALL_DATE_SETTING, System.currentTimeMillis());

            // Don't forget to commit your edits!!!
            editor.commit();
        } catch (Exception ex) {

        }
    }

    public static int getMinNumberRange(Context ctxt) {
        int value = 1;
        try {
            SharedPreferences settings = PreferenceManager
                    .getDefaultSharedPreferences(ctxt);
            value = settings.getInt(KEY_MIN_NO_RANGE_SETTING, value);
        } catch (Exception ex) {

        }
        return value;
    }
    public static void setMinNumberRange(Context ctxt, int value) {
        try {
            // Save user preferences. We need an Editor object to
            // make changes. All objects are from android.context.Context
            SharedPreferences settings = PreferenceManager
                    .getDefaultSharedPreferences(ctxt);

            SharedPreferences.Editor editor = settings.edit();
            editor.putInt(KEY_MIN_NO_RANGE_SETTING, value);

            // Don't forget to commit your edits!!!
            editor.commit();
        } catch (Exception ex) {

        }
    }


}
