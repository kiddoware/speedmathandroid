package com.kiddoware.speedmathandroid;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Button b_settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Utility.getFirstTimeSetting(getApplicationContext())){
            Intent intent= new Intent(MainActivity.this, SetActivity.class);
            startActivity(intent);
            Utility.setFirstTimeSetting(getApplicationContext(),false);

        }
        else {
            setContentView(R.layout.activity_main);

            b_settings = (Button) findViewById(R.id.b_settings);
        }


    }

    public void OnSetClick(View v){

        Intent intent= new Intent(MainActivity.this, SetActivity.class);
        startActivity(intent);
    }

    public void onAddClick(View v) {
        Intent myIntent = new Intent(MainActivity.this, MathActivity.class);
        Bundle b = new Bundle();
        b.putString("operation","Add");
        myIntent.putExtras(b);
        startActivity(myIntent);
    }
    public void onSubtractClick(View v) {
        Intent myIntent = new Intent(MainActivity.this, MathActivity.class);
        Bundle b = new Bundle();
        b.putString("operation","Subtract");
        myIntent.putExtras(b);
        startActivity(myIntent);
    }
    public void onMultiplicationClick(View v) {
        Intent myIntent = new Intent(MainActivity.this, MathActivity.class);
        Bundle b = new Bundle();
        b.putString("operation","Multiplication");
        myIntent.putExtras(b);
        startActivity(myIntent);
    }
    public void onDivisionClick(View v) {
        Intent myIntent = new Intent(MainActivity.this, MathActivity.class);
        Bundle b = new Bundle();
        b.putString("operation","Division");
        myIntent.putExtras(b);
        startActivity(myIntent);
    }

}
